[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Primera Práctica
Para la resolución de esta primera práctica se utilizará como herramienta de concurrencia los semáforos. Para el análisis y diseño el uso de semáforos es como se ha visto en las clases de teoría. Para la implementación se hará uso de la clase [`Semaphore`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html) de JAVA. Hay diferentes ejemplos en este [guión](https://gitlab.com/ssccdd/materialadicional/-/blob/master/README.md) donde se demuestra el uso general de la clase. Para la implementación se se utilizarán las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución concurrente.

## Problema a resolver
Tenemos un sistema que dispone de una capacidad de recursos reutilizables quede deberán ser asignados a los procesos para que completen sus operaciones. El número de recursos estará determinado por la constante `NUM_RECURSOS` (20). Los elementos presentes para la solución serán los siguientes:

 - Un **monitor** para las peticiones de acceso a los recursos: Será el encargado de centralizar las peticiones que deben realizar los procesos y el gestor para operar con los recursos presentes en el sistema. Deberá satisfacer las siguientes restricciones.

	- Ningún proceso puede tener asignados más de 4 recursos.
    
	- Para la petición inicial de un proceso se le deben asignar siempre 2 recursos disponibles. Si no es posible se registrará el fallo correspondiente.
    
	- Si no se le pueden asignar más recursos se sustituirá la tarea que lleve más tiempo utilizando un recurso para ese proceso. Deberá registrarse esta sustitución.

- **Los procesos**: Para simular el comportamiento de los procesos en el sistema  se seguirán los siguientes pasos: 

	- Solicitará al monitor que se le asignen los recursos al inicio de sus operaciones. Inicialmente se le asignan dos recursos para las tareas 1 y 2 del proceso.
	
	- Realizará un número variable de ciclos de operaciones, entre 8 y 12. Ese número simula las operaciones que debe realizar el proceso para completar su ejecución.
    
	- Para cada operación se simulará que necesita un tiempo de entre 1 y 2 segundos:
    
	    -  Genera un número de tarea de entre las disponibles del proceso a la que se le asigna un recurso de los presentes en el sistema.
        
	    -  Pregunta al monitor si la tarea tiene asignado ya un recurso.
        
	    -  Si la tarea no tiene asignado un recurso se solicitará al monitor que le asigne un recurso.
        
	- Cuando se complete el último ciclo de operación finalizará el proceso. Antes de finalizar deberá comunicar al monitor que libera los recursos que tenga asignados.

- **Gestor de recursos**: Será el encargado de simular el comportamiento del gestor de recursos que seguirá los siguientes pasos:
    - Comprueba en el monitor las peticiones pendientes de liberación de recursos. La petición no puede bloquear al gestor de recursos si no hay peticiones pendientes.
        - Atiende todas las peticiones pendientes.
    
    - Comprueba en el monitor las peticiones pendientes de asignación de recursos que han realizado los procesos al monitor. La petición no puede bloquear al gestor de recursos si no hay peticiones pendientes.
        - Atiende una petición cada vez.

- **El sistema**: La simulación del comportamiento del sistema se realizará mediante el programa principal y tendrá los siguientes pasos:

	- Creará y ejecutará el gestor de recursos como primera tarea.
    
	- Creará y ejecutará un nuevo proceso en un intervalo comprendido entre 1 y 3 segundos. Para cada proceso se generará un número aleatorio de tareas que tiene un proceso y estará comprendido entre 4 y 8.
    
	- El tiempo de ejecución del sistema será de 2 minutos. Pasado ese tiempo finalizará los procesos que aún están activos y el gestor de procesos.
    
	- El sistema presentará los siguientes datos antes de finalizar:
		- Para cada proceso:
			- Tiempo de inicio.
			- Tiempo de ejecución.
			- Número de fallos en la asignación de recursos. Ya sea a su inicio o por sustitución para sus tareas.  
		- Mostrará el número de procesos que no han concluido su ejecución. 
		- La asignación media de recursos a los procesos.
		- La media de fallos de asignación de recursos a los procesos.

**NOTA**: El inicio del proceso es cuando solicita al monitor la asignación inicial de recursos. La finalización del proceso es cuando completa la solicitud al monitor para devolver los recursos asignados. Se deben asignar los recursos pedidos por los procesos, una vez iniciada su ejecución, es decir, si no hay disponibilidad se deberán satisfacer en cuanto haya disponibilidad. Se deberán definir todas las constantes simbólicas necesarias para la solución del problema. Para la solución no deben incluirse semáforos en el monitor.

# Análisis

Antes de nada tenemos que crear una serie de constantes en la clase monitor, donde se almacenarán las variables compartidas:
```
int NUM_RECURSOS = 20;
int NUM_MAX_RECURSOS = 4;
int NUM_MIN_RECURSOS = 2;
```
Habrá que definir también nuestro buffer de recursos:
```
Recurso buffer_recursos{
    Variables:
        Elemento elemento_buffer;
    Funciones:
        void add(elemento_buffer): Se añadirá un nuevo elemento al final del buffer
        void get(posicion): Saca un elemento del buffer
        void tieneRecursoDisponible(tarea): Revisa si la tearea que se pasa tiene recurso disponible
        Elemento sacar_elemento(): Se mostrará el primer elemento y posteriormente se eliminará
} 
```


Para resolver este problema vamos a compararlo con el problema clásico de productores y consumidores. En este caso, el proceso Gestor de recursos será el productor ya que es el que modifica los recursos del buffer, y el proceso monitor será el consumidor porque será el proceso encargado de leer el buffer.

En el problema del productor/consumidor con buffer limitado hay un número fijo de posiciones en el buffer, como en este problema. En este caso, el consumidor (Monitor) tiene que esperar si todas las posiciones del buffer se encuentran vacias, y el productor (Gestor de recursos) tiene que esperar si todas las posiciones del buffer estan llenas.

## Semáforos

Para resolver este problema, utilizaremos los siguientes semáforos:

- **Semáforos de exclusión mutua**
semáforo mutExBuffer(1): Nos permitirá controlar el acceso en exclusión mútua al buffer, y se inicializará a 1.

- **Semáforos de sincronización**

semáforo vacios(NUM_RECURSOS): Nos permitirá contar el número de posiciones vacias del buffer, y se inicializará a NUM_RECURSOS. Cuando no queden posiciones vacias dentro del buffer, este semáforo tendrá el valor 0, con lo que cualquier productor (Gestor de recursos) que intente acceder al buffer será bloqueado.

semáforo llenos(0): Llevará la cuenta del número de posiciones llenas del buffer, y se inicializará a 0. Nos permitirá bloquear a los consumidores cuando no existan elementos en el buffer.



# Diseño

El pseudocódigo para los distintos procesos del sistema es:

### Sistema
```
Proceso Sistema()

Variables 
	GestorSistema gestorSistema

ejecucionProceso { // Hilo de ejecución del Sistema
	gestorSistema = GestorSistema()
	tiempoInicial = tiempoActual() // Obtengo la hora actual para luego calcular los 2 minutos
	tiempoFinal = tiempoInicial + 120 // Calcula la hora en la que parará el sistema

	gestorSistema.ejecucionProceso() // Lanzo hilo del gestor

	// Genero procesos en la ventana temporal de 2min
	while (tiempoActual() < tiempoFinal) {
		nuevoProceso = Proceso(random(4, 8)) // Creo un nuevo proceso con entre 4 y 8 tareas
		gestorSistema.addProcesoNuevo(nuevoProceso)
		sleep(random(1, 3)) // Duermo el proceso en un tiempo aleatorio entre 1 y 3 segundos
	}

	// calcular estadisticas
    for(int i = 0; i < gestor.getProcesos().size(); i++) {
            gestor.getProcesos().get(i).mostrarEstadisticas()
    }
    return 0

}
```

### GestorRecursos
```
Proceso GestorRecursos()

Variables
	Buffer buffer
	Monitor monitor
	Proceso[] procesos

addProcesoNuevo(nuevoProceso) {
	procesos.add(nuevoProceso)
	nuevoProceso.ejecucionProceso(&monitor) // Lanzo el hilo del proceso, pasandole una referencia del monitor
}

ejecucionProceso() { // Hilo de ejecución del Gestor de recursos
	monitor = Monitor(&buffer) // Se pasa el buffer por referencia para que pueda consumirlo
	monitor.ejecucionProceso() // Lanzo hilo del monitor

	while(true) {
		liberacionRecursos()
		asignacionRecursos()
	}
}

liberacionRecursos() {
    peticiones = monitor.getPeticionesLiberacion()
    for(int i = 0; i < peticiones.size(); i++) {
        wait(llenos)
        wait(mutExBuffer)
        buffer.get(peticiones.get(i)) = 0) // Suponemos que liberar el buffer es poner el recurso a 0
        signal(mutExBuffer)
        signal(vacios)
    }
}

asignacionRecursos() {
    if(monitor.getPeticionesRecursos().size() > 0) {
        peticion = monitor.getPeticionesRecursos()[0] // Cogemos siempre la primera peticion posible
        monitor.getPeticionesRecursos().pop(0)
        if (buffer.tieneRecursoDisponible()) {
            wait(vacios)
            wait(mutExBuffer)
            buffer.getRecursoDisponible() = peticion
            signal(mutExBuffer)
            signal(llenos)
        }
    }
}

```

### Monitor
```
Proceso Monitor(&buffer)

Variables
    Peticion[] peticionesRecursos
    Peticion[] peticionesLiberacion
    
ejecucionProceso() { // Hilo de ejecución del Monitor
    while(true) {
        // El proceso sigue en ejecución, esperando a que le lleguen peticiones
    }
}

addPeticionesRecursosIniciales(tarea1, tarea2) {
    peticionesRecursos.add(tarea1)
    peticionesRecursos.add(tarea2)
}

addPeticionRecursos(tarea) {
    peticionesRecursos.add(tarea)
}

addPeticionLiberar(tarea) {
    peticionesLiberacion.add(tarea)
}

getPeticionesRecursos() {
    return peticionesRecursos
}

getPeticionesLiberacion() {
    return peticionesLiberacion
}

tareaConRecurso(tarea) {
    for(int i = 0; i < buffer.size(); i++) {
        wait(llenos)
        wait(mutExBuffer)
        if(buffer.get(i) == tarea) {
            return true
        }
        signal(mutExBuffer)
        signal(vacios)
    }
    return false
 }

```

### Proceso
```
Proceso Proceso(nuevasTareas) {
    for(int i = 0; i < nuevasTareas.size(); i++) {
        tareas.add(tareas)
     }
 }

Variables
    Tarea[] tareas
    Fallo[] fallos
    Susticion[] sustituciones

ejecucionProceso(&monitor) { // Hilo de ejecución del Proceso, tiene una referencia al monitor
    peticionInicialRecursos(&monitor)
    
    int nOperaciones = random(8, 12)
    int i = 0
    
    while(i < nOperaciones) {
        Tarea nuevaTarea = tareas.get(random(0, tareasSinRecursos.size()))
        boolean recursoYaAsignado = monitor.tareaConRecurso(nuevaTarea)
        if(!recursoYaAsignado) {
            monitor.addPeticionRecursos(nuevaTarea)
        }
        sleep(random(1,2))
    }
    monitor.liberarTareas(tareas)
}

peticionInicialRecursos(&monitor) {
    monitor.addPeticionesRecursosIniciales(tareas[0], tareas[1])
}

mostrarEstadisticas() { // En esta función mostramos por pantalla las estadísticas del proceso

    /*
    - Tiempo de inicio.
    - Tiempo de ejecución.
    - Número de fallos en la asignación de recursos. Ya sea a su inicio o por sustitución para sus tareas.  
    - Mostrará el número de procesos que no han concluido su ejecución. 
    - La asignación media de recursos a los procesos.
    - La media de fallos de asignación de recursos a los procesos.
    */
}

```
  
