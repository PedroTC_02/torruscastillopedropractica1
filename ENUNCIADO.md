
﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Primera Práctica
Para la resolución de esta primera práctica se utilizará como herramienta de concurrencia los semáforos. Para el análisis y diseño el uso de semáforos es como se ha visto en las clases de teoría. Para la implementación se hará uso de la clase [`Semaphore`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html) de JAVA. Hay diferentes ejemplos en este [guión](https://gitlab.com/ssccdd/materialadicional/-/blob/master/README.md) donde se demuestra el uso general de la clase. Para la implementación se se utilizarán las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución concurrente.

## Problema a resolver
Tenemos un sistema que dispone de una capacidad de recursos reutilizables quede deberán ser asignados a los procesos para que completen sus operaciones. El número de recursos estará determinado por la constante `NUM_RECURSOS` (20). Los elementos presentes para la solución serán los siguientes:

 - Un **monitor** para las peticiones de acceso a los recursos: Será el encargado de centralizar las peticiones que deben realizar los procesos y el gestor para operar con los recursos presentes en el sistema. Deberá satisfacer las siguientes restricciones.

	- Ningún proceso puede tener asignados más de 4 recursos.
    
	- Para la petición inicial de un proceso se le deben asignar siempre 2 recursos disponibles. Si no es posible se registrará el fallo correspondiente.
    
	- Si no se le pueden asignar más recursos se sustituirá la tarea que lleve más tiempo utilizando un recurso para ese proceso. Deberá registrarse esta sustitución.

- **Los procesos**: Para simular el comportamiento de los procesos en el sistema  se seguirán los siguientes pasos: 

	- Solicitará al monitor que se le asignen los recursos al inicio de sus operaciones. Inicialmente se le asignan dos recursos para las tareas 1 y 2 del proceso.
	
	- Realizará un número variable de ciclos de operaciones, entre 8 y 12. Ese número simula las operaciones que debe realizar el proceso para completar su ejecución.
    
	- Para cada operación se simulará que necesita un tiempo de entre 1 y 2 segundos:
    
	    -  Genera un número de tarea de entre las disponibles del proceso a la que se le asigna un recurso de los presentes en el sistema.
        
	    -  Pregunta al monitor si la tarea tiene asignado ya un recurso.
        
	    -  Si la tarea no tiene asignado un recurso se solicitará al monitor que le asigne un recurso.
        
	- Cuando se complete el último ciclo de operación finalizará el proceso. Antes de finalizar deberá comunicar al monitor que libera los recursos que tenga asignados.

- **Gestor de recursos**: Será el encargado de simular el comportamiento del gestor de recursos que seguirá los siguientes pasos:
	- Comprueba en el monitor las peticiones pendientes de liberación de recursos. La petición no puede bloquear al gestor de recursos si no hay peticiones pendientes.
		- Atiende todas las peticiones pendientes.
	
	- Comprueba en el monitor las peticiones pendientes de asignación de recursos que han realizado los procesos al monitor. La petición no puede bloquear al gestor de recursos si no hay peticiones pendientes.
		- Atiende una petición cada vez.

- **El sistema**: La simulación del comportamiento del sistema se realizará mediante el programa principal y tendrá los siguientes pasos:

	- Creará y ejecutará el gestor de recursos como primera tarea.
    
	- Creará y ejecutará un nuevo proceso en un intervalo comprendido entre 1 y 3 segundos. Para cada proceso se generará un número aleatorio de tareas que tiene un proceso y estará comprendido entre 4 y 8.
    
	- El tiempo de ejecución del sistema será de 2 minutos. Pasado ese tiempo finalizará los procesos que aún están activos y el gestor de procesos.
    
	- El sistema presentará los siguientes datos antes de finalizar:
		- Para cada proceso:
			- Tiempo de inicio.
			- Tiempo de ejecución.
			- Número de fallos en la asignación de recursos. Ya sea a su inicio o por sustitución para sus tareas.  
		- Mostrará el número de procesos que no han concluido su ejecución. 
		- La asignación media de recursos a los procesos.
		- La media de fallos de asignación de recursos a los procesos.

**NOTA**: El inicio del proceso es cuando solicita al monitor la asignación inicial de recursos. La finalización del proceso es cuando completa la solicitud al monitor para devolver los recursos asignados. Se deben asignar los recursos pedidos por los procesos, una vez iniciada su ejecución, es decir, si no hay disponibilidad se deberán satisfacer en cuanto haya disponibilidad. Se deberán definir todas las constantes simbólicas necesarias para la solución del problema. Para la solución no deben incluirse semáforos en el monitor.
